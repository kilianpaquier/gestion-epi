<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 21/03/2019
 * Time: 12:20
 */

session_start();
if (!isset($_SESSION['certificat'])) {
    header("location:/login");
    exit();
}
