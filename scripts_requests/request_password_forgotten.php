<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 20/03/2019
 * Time: 11:07
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$email = $_POST["mail"];
$certificat = $_POST['certificat'];

$query = $bdd->prepare("SELECT mail FROM verificateurs where mail = ? and certificat = ?");
$query->execute(array($email, $certificat));

if ($query->rowCount() == 1) {
    $mailaff['mail'] = $email;
    echo json_encode($mailaff);
} else {
    $mailaff['mail'] = "false";
    echo json_encode($mailaff);
}
