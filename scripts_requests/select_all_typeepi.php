<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 20/03/2019
 * Time: 09:30
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

ob_get_clean(); //pour clean echo
$sql = "SELECT libelle FROM type_epi";
$query = $bdd->prepare($sql);
$query->execute();
$type_epi = $query->fetchAll();

echo json_encode($type_epi);