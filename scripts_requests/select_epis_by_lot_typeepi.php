<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 15/04/2019
 * Time: 12:12
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select numero_serie from epi where lot = ? and type_epi = ? and (if (exists(select etat from verifications where epi = numero_serie order by date_verification desc limit 1), (select etat from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) != 'Rebut'");
$query->execute(array($_POST['lot'], $_POST['type_epi']));
$epi = $query->fetchAll();
ob_get_clean(); //pour clean echo
echo json_encode($epi);