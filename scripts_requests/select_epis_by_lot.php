<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 14/05/2019
 * Time: 18:09
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select numero_serie from epi where lot = ?");
$query->execute(array($_POST['lot']));
$epi = $query->fetchAll();
ob_get_clean(); //pour clean echo
echo json_encode($epi);