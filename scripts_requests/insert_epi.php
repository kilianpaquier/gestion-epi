<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 20/03/2019
 * Time: 09:47
 */

include("../application/controllers/Connexion.php");
include("../application/controllers/FtpController.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$numero_serie = $_POST['numero_serie'];
$marque = $_POST['marque'];
$modele = $_POST['modele'];
$annee_fabrication = $_POST['annee_fabrication'];
$date_mise_en_service = $_POST['date_mise_en_service'];
$date_fin_de_vie = $_POST['date_fin_vie'];
$code_couleur = $_POST['code_couleur'];
$lot = $_POST['lot'];
$type_epi = $_POST['type_epi'];
$groupe = $_POST['groupe'];

$retour = array();

// On créé le lot s'il n'existe pas
$queryLot = $bdd->prepare("SELECT nom_lot FROM lot WHERE nom_lot = ?");
$queryLot->execute(array($lot));
if ($queryLot->rowCount() == 0) {
    $queryLot = $bdd->prepare("INSERT INTO lot (nom_lot, code_couleur, groupe) VALUE (?, ?, ?)");
    $retour['insert'] = $queryLot->execute(array($lot, $code_couleur, $groupe));
    if (!$retour['insert']) {
        ob_get_clean();
        echo json_encode($retour);
        exit();
    }
    mkdir(dirname(__DIR__,1) . "/pdf_verifications/" . str_replace(" ", "_", $groupe) . "/" . str_replace(" ", "_", $lot));
}

// On créé la marque si elle n'existe pas
$queryLot = $bdd->prepare("SELECT nom from marque where nom = ?");
$queryLot->execute(array($marque));

if ($queryLot->rowCount() == 0) {
    $queryLot = $bdd->prepare("INSERT INTO marque (nom) value (?)");
    $queryLot->execute(array($marque));
}

try {
    if (strcmp("true", $_POST['isFile']) == 0) {

        $image = $_FILES['image'];
        $filename = "/images_epi/" . $numero_serie . "." . pathinfo($image['name'])['extension'];

        if ($type_epi == "Corde d'accès" || $type_epi == "Corde de rappel") {

            $diametre = $_POST['diametre'];
            $nb_epissure = $_POST['nb_epissure'];
            $longueur_corde = $_POST['longueur_corde'];

            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie, diametre,
                 nb_epissure, image, lot, type_epi, longueur_corde) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $diametre,
                $nb_epissure,
                $_SERVER['SERVER_NAME'] . $filename,
                $lot,
                $type_epi,
                $longueur_corde
            ));
        } elseif ($type_epi == "Longe armée" || $type_epi == "Longe cordée") {

            $numero_tendeur_reducteur = $_POST['numero_tendeur_reducteur'];
            $numero_connecteur_extremite = $_POST['numero_connecteur_extremite'];

            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie,
                 numero_tendeur_reducteur, numero_connecteur_extremite, image, lot, type_epi) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $numero_tendeur_reducteur,
                $numero_connecteur_extremite,
                $_SERVER['SERVER_NAME'] . $filename,
                $lot,
                $type_epi
            ));
        } else {
            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie,
                image, lot, type_epi) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $_SERVER['SERVER_NAME'] . $filename,
                $lot,
                $type_epi
            ));
        }

        if ($retour['success'] == true) {
            if (file_exists(dirname(__DIR__, 1) . $filename)) {
                unlink(dirname(__DIR__, 1) . $filename);
            }
            $moved = move_uploaded_file($image['tmp_name'], dirname(__DIR__, 1) . $filename);

            if (!$moved) {
                ob_get_clean();
                $retour['success'] = "Echec de l'importation du fichier";
                echo json_encode($retour);
                exit();
            }
        }
    } else {
        if ($type_epi == "Corde d'accès" || $type_epi == "Corde de rappel") {

            $diametre = $_POST['diametre'];
            $nb_epissure = $_POST['nb_epissure'];
            $longueur_corde = $_POST['longueur_corde'];

            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie, diametre,
                 nb_epissure, lot, type_epi, longueur_corde) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $diametre,
                $nb_epissure,
                $lot,
                $type_epi,
                $longueur_corde
            ));
        } elseif ($type_epi == "Longe armée" || $type_epi == "Longe cordée") {

            $numero_tendeur_reducteur = $_POST['numero_tendeur_reducteur'];
            $numero_connecteur_extremite = $_POST['numero_connecteur_extremite'];

            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie,
                 numero_tendeur_reducteur, numero_connecteur_extremite, lot, type_epi) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $numero_tendeur_reducteur,
                $numero_connecteur_extremite,
                $lot,
                $type_epi
            ));
        } else {
            $stmt = $bdd->prepare("INSERT INTO epi (numero_serie, marque, modele, annee_fabrication, date_mise_en_service, date_fin_de_vie,
                lot, type_epi) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

            $retour['success'] = $stmt->execute(array(
                $numero_serie,
                $marque,
                $modele,
                $annee_fabrication,
                $date_mise_en_service,
                $date_fin_de_vie,
                $lot,
                $type_epi
            ));
        }
    }

    ob_get_clean();
    echo json_encode($retour);
} catch (PDOException $exception) {
    ob_get_clean();
    $retour['success'] = $exception->getMessage();
    echo json_encode($retour);
}

