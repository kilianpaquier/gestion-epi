<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 23/04/2019
 * Time: 16:41
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();
$nom_lot = $_POST['nom_lot'];
$numero_serie = $_POST['numero_serie'];
$retour = array();

$query = $bdd->prepare("UPDATE epi SET lot = ? WHERE numero_serie = ?");
$retour['success'] = $query->execute(array($nom_lot, $numero_serie));
ob_get_clean();
echo json_encode($retour);