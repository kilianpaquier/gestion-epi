<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 16/05/2019
 * Time: 11:42
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("SELECT *, (select l.code_couleur from lot as l where l.nom_lot = epi.lot) as code_couleur FROM epi WHERE numero_serie = ?");
$query->execute(array($_POST['numero_serie']));

$result = $query->fetchAll();

ob_get_clean();
echo json_encode($result[0]);