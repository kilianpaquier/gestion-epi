<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 16/03/2019
 * Time: 22:30
 */

include("../application/controllers/Connexion.php");
ob_get_clean();

$mail = $_POST['mail'];
$password = hash("sha256", $_POST['password']);

$to = $mail;
$email_subject = "Gestion d'EPI - Votre mot de passe provisoire";
$email_body = "Bonjour <br/><br/>" . "Vous avez demandé une réinitialisation de votre mot de passe sur l'application de gestion d'EPI.<br/>" .
    "Voici un mot de passe provisoire, <b>veuillez le changer dans les plus brefs délais</b> : " . $_POST['password'] .
    "<br/><br/>Ceci est un message généré automatiquement, merci de ne pas y répondre.<br/><br/>Cordialement,<br/>Postmaster, gestion des EPI";
$header = "MIME-Version: 1.0" . "\r\n";
$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$header .= "From : postmaster@gestion-epi.org";

$result = array();

try {
    $result['mail'] = mail($to, $email_subject, $email_body, $header);

    $bdd = \controler\connexion\Connexion::getInstance()->getBdd();

    if ($result['mail']) {
        $query = $bdd->prepare("UPDATE verificateurs SET mot_de_passe = ? where mail = ?");
        $result['success'] = $query->execute(array($password, $mail));

        echo json_encode($result);
    } else {
        $result['success'] = false;
        echo json_encode($result);
    }

} catch (Exception $exception) {
    $result['success'] = $exception->getMessage();
    echo json_encode($result);
}



