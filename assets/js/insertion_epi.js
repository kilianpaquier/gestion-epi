/**
 * Fonction vérifiant les champs d'insertion de l'EPI et qui affiche un modal pour la confirmation
 * de l'insertion
 */
function beforeCreateEpi() {
    event.preventDefault();

    /**
     * On récupère toutes les informations de l'EPI
     */

    let groupe = $("#nom_groupe").val();

    let lot = $("#lot").val();
    let typeEpi = $("#epi").val();
    let numeroSerie = $("#numero_serie").val();
    let marque = $("#marqueInput").val();

    let fabricationDate = $("#annee_fabrication").val();
    let inServiceDate = $("#date_mise_en_service").val();
    let endDate = $("#fin_de_vie").val();
    let modele = $("#modele").val();
    let image = $("#file-with-current").prop("files")[0];
    let colorCode = $("#code_couleur").val();

    let cordeLength = $("#longueur_corde").val();
    let nbEpissure = $("#nombre_epissure").val();
    let diametre = $("#diametre").val();
    let tendeurReducteurNumber = $("#num_tendeur").val();
    let connecteurExtremiteNumber = $("#numero_connecteur").val();

    console.log(image);
    if (image !== undefined)
        image = image.name;
    else
        image = "Pas d'image";

    /**
     * On vérifié si tous les champs ont été saisis
     */
    if (lot === "" || typeEpi === "" || numeroSerie === "" || marque === "" || fabricationDate === "" ||
        inServiceDate === "" || modele === "" || colorCode === "") {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("L'un des champs obligatoire n'a pas été saisi");
        });
        return false;
    }

    /**
     * On vérifie les champs si l'EPI est une corde
     */
    if ((typeEpi === "Corde d'accès" || typeEpi === "Corde de rappel") && ((diametre === "") || nbEpissure === "" || cordeLength === "")) {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("L'un des champs spéciaux pour les cordes n'a pas été saisi");
        });
        return false;
    }

    /**
     * On vérifie les champs si l'EPI est une longe
     */
    if ((typeEpi === "Longe armée" || typeEpi === "Longe cordée") &&
        (tendeurReducteurNumber === "" || connecteurExtremiteNumber === "")) {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("L'un des numéros pour les longes n'a pas été saisi");
        });
        return false;
    }

    if (lot === 'Stock' || lot === "stock") {
        groupe = 'Stock';
    }

    /**
     * On remplit le résumé de l'insertion pour l'afficher
     */
    $("#writeInfo").empty();
    $("#writeInfo").append(
        '<div class="row" id="dataEpi">' +
        '<div class="col-12 col-lg-6"><label>Groupe : </label> ' + groupe +
        '</div><div class="col-12 col-lg-6"><label>Type d\'EPI : </label> ' + typeEpi +
        '</div><div class="col-12 col-lg-6"><label>Marque : </label> ' + marque +
        '</div><div class="col-12 col-lg-6"><label>Identification du lot : </label> ' + lot +
        '</div><div class="col-12 col-lg-6"><label>Code couleur : </label> ' + colorCode +
        '</div><div class="col-12 col-lg-6"><label>Identification individuelle : </label> ' + numeroSerie +
        '</div><div class="col-lg-6 col-12"><label>Date de fabrication : </label> ' + fabricationDate +
        '</div><div class="col-lg-6 col-12"><label>Date de mise en service : </label> ' + inServiceDate +
        '</div><div class="col-12 col-lg-6"><label>Date de fin de vie : </label> ' + endDate +
        '</div><div class="col-lg-6 col-12"><label>Modèle : </label> ' + modele +
        '</div><div class="col-lg-6 col-12"><label>Image : </label> ' + image +
        '</div>' +
        '</div>'
    );

    if (typeEpi !== "Corde d'accès" && typeEpi !== "Corde de rappel") {
        diametre = null;
        cordeLength = null;
        nbEpissure = null;
    } else {
        $("#dataEpi").append(
            '<div class="col-12 col-lg-6"><label>Nombre d\épissures : </label> ' + nbEpissure +
            '</div><div class="col-lg-6 col-12"><label>Diamètre : </label> ' + diametre +
            '</div><div class="col-lg-6 col-12"><label>Longueur de la corde : </label> ' + cordeLength +
            '</div>'
        )
    }
    if (typeEpi !== "Longe armée" && typeEpi !== "Longe cordée") {
        connecteurExtremiteNumber = null;
        tendeurReducteurNumber = null;
    } else {
        // TODO ajouter dans le modal l'affichage des deux connecteurs
    }

    $("#exampleModalPreview").modal("show");

    $("#submitLink").on("click", function () {
        createEpi();
    })
}

$(document).ready(function () {
    /**
     * Chargement des champs depuis la BD
     */
    loadFields();

    /**
     * Initialisation du datepicker pour choisir la date de fabrication
     */
    $(".fabricationDate").datepicker({
        format: "mm-yyyy",
        startView: 1,
        minViewMode: 1,
        maxViewMode: 2,
        language: "fr",
        autoclose: true,
        forceParse: 1,
        startDate: new Date(1999, 0, 1),
    }).on("change", function () {
        /**
         * Lorsque le champs date de fabrication change on actualise les contraintes pour les autres dates
         */
        onChangeFabrication();
    });

    /**
     * On initialise le datepicker pour la date de fin de vie
     */
    $(".endLifeDate").datepicker({
        format: "mm-yyyy",
        startView: 1,
        maxViewMode: 2,
        minViewMode: 1,
        language: "fr",
        autoclose: true,
        todayHighlight: true,
        forceParse: 1,
    }).on("change", function () {
        /**
         * Lorsque le champs date de fin de vie change on actualise les contraintes pour les autres dates
         */
        onChangeEnd()
    });

    /**
     * On initialise le datepicker pour la date de mise en service
     */
    $(".serviceDate").datepicker({
        maxViewMode: 2,
        startView: 1,
        minViewMode: 1,
        language: "fr",
        autoclose: true,
        format: "mm-yyyy",
        todayHighlight: true,
        forceParse: 1,
    }).on("change", function () {
        /**
         * Lorsque la date de mise en service change on actualise les contraintes pour les autres dates
         */
        onChangeService()
    });

    /**
     * Lorsque l'on change le type d'EPI on actualise les champs affichés (pour les longes et cordes en particulier)
     */
    $("#epi").on('change', function () {
        updateDisplayedFields();
    });

    /**
     * On initialise l'action d'ajout dans la BD
     */
    $("#submitButton").on('click', function () {
        //createEpi();

        beforeCreateEpi();
    });

    setTimeout(function () {
        let oldValueMarque = "";
        $("#marqueInput").on("change", function () {
            let marque = $("#marqueInput").val();
            if (marque !== "" && oldValueMarque !== marque) {
                if (marque.length > 18)
                    marque = marque.substr(0, marque.length - 1);

                let firstLetter = marque.charAt(0).toUpperCase();
                let string = marque.slice(1);

                oldValueMarque = firstLetter + string;

                $("#marqueInput").val(firstLetter + string);
            }
        });

        let oldValueLot = "";
        $("#lot").on("change", function () {
            let lot = $("#lot").val();
            if (lot !== "" && oldValueLot !== lot) {
                // On empêche le / pour la création des dossiers côté serveur
                if (lot.slice(-1) === '/' || lot.slice(-1) === '\\')
                    lot = lot.substr(0, lot.length - 1);
                if (lot.length > 18)
                    lot = lot.substr(0, lot.length - 1);

                let firstLetter = lot.charAt(0).toUpperCase();
                let string = lot.slice(1);

                oldValueLot = firstLetter + string;

                $("#lot").val(firstLetter + string);
            }
        });
    }, 300);

    let oldValueColor = "";
    $("#code_couleur").on("keyup", function () {
        let colorCode = $("#code_couleur").val();
        if (colorCode !== "" && oldValueColor !== colorCode) {
            let firstLetter = colorCode.charAt(0).toUpperCase();
            let string = colorCode.slice(1);

            oldValueColor = firstLetter + string;

            $("#code_couleur").val(firstLetter + string);
        }
    });

    let oldValueModele = "";
    $("#modele").on("keyup", function () {
        let modele = $("#modele").val();
        if (modele !== "" && oldValueModele !== modele) {
            let firstLetter = modele.charAt(0).toUpperCase();
            let string = modele.slice(1);

            oldValueModele = firstLetter + string;

            $("#modele").val(firstLetter + string);
        }
    });

    $("#numero_serie").on("keypress", function () {
        if (event.code === "Space")
            return false;
    });

    $("#file-with-current").on("change", function () {
        if ($("#file-with-current").prop("files")[0] !== undefined) {
            let size = $("#file-with-current").prop("files")[0].size;
            if (size > 5242880) {
                $.getScript("assets/js/utils.js", function () {
                    displayTemporaryErrorMessage("La taille de l'image est trop importante, 5Mo maximum");
                });
                $(this).val("");
                $(".span-choose-file").text("Image de l'EPI");
            }
        } else {
            $(this).val("");
            $(".span-choose-file").text("Image de l'EPI");
        }
    });

    lotExists($("#lot").val());
});

/**
 * On vérifie si la touche saisie est bien un chiffre pour les champs ne pouvant contenir que des chiffres
 * @param event
 * @returns {boolean}
 */
function isNumber(event) {
    let charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

/**
 * Fonction qui change les restriction pour les dates en fonction de la date de fabrication
 */
function onChangeFabrication() {
    let serviceInput = $(".serviceDate");
    let endInput = $(".endLifeDate");

    let fabricationText = $(".fabricationDate").val();
    let serviceText = serviceInput.val();
    let endText = endInput.val();

    let dateFabrication = new Date(fabricationText.split("-")[1], fabricationText.split("-")[0] - 1, 1);
    let serviceDate = new Date(serviceText.split("-")[1], serviceText.split("-")[0] - 1, 1);
    let endLifeDate = new Date(endText.split("-")[1], endText.split("-")[0] - 1, 1);

    /**
     * On met à jour la restriction pour la date de mise en service
     */
    serviceInput.datepicker("remove");
    serviceInput.datepicker({
        maxViewMode: 2,
        startView: 1,
        minViewMode: 1,
        language: "fr",
        autoclose: true,
        format: "mm-yyyy",
        todayHighlight: true,
        forceParse: 1,
        startDate: dateFabrication,
        endDate: endLifeDate,
    });

    /**
     * Si la date de mise en service n'a pas été saisie alors la date de minimale pour la date de fin de vie
     * est la date de fabrication
     */
    if (serviceText === "") {
        endInput.datepicker("remove");
        endInput.datepicker({
            maxViewMode: 2,
            startView: 1,
            minViewMode: 1,
            language: "fr",
            autoclose: true,
            format: "mm-yyyy",
            todayHighlight: true,
            forceParse: 1,
            startDate: dateFabrication,
        });
    }
    /**
     * Sinon la date de minimale pour la date de fin de vie sera la date de mise en service
     */
    else if (serviceDate > dateFabrication) {
        endInput.datepicker("remove");
        endInput.datepicker({
            maxViewMode: 2,
            startView: 1,
            minViewMode: 1,
            language: "fr",
            autoclose: true,
            format: "mm-yyyy",
            todayHighlight: true,
            forceParse: 1,
            startDate: serviceDate,
        });
    }
}

/**
 * Fonction qui met à jour les restrictions pour les dates en fonction de la date de mise en service
 */
function onChangeService() {
    let serviceInput = $(".serviceDate");
    let endInput = $(".endLifeDate");
    let fabricationInput = $(".fabricationDate");

    let serviceText = serviceInput.val();

    let serviceDate = new Date(serviceText.split("-")[1], serviceText.split("-")[0] - 1, 1);

    /**
     * On met à jour la restriction sur la date de fabrication
     */
    fabricationInput.datepicker("remove");
    fabricationInput.datepicker({
        format: "mm-yyyy",
        startView: 1,
        minViewMode: 1,
        maxViewMode: 2,
        language: "fr",
        autoclose: true,
        forceParse: 1,
        endDate: serviceDate,
    });

    /**
     * On met à jour la restriction pour la date de fin de vie
     */
    endInput.datepicker("remove");
    endInput.datepicker({
        maxViewMode: 2,
        startView: 1,
        minViewMode: 1,
        language: "fr",
        autoclose: true,
        format: "mm-yyyy",
        todayHighlight: true,
        forceParse: 1,
        startDate: serviceDate,
    });
}

/**
 * Fonction qui met à jour les dates en fonction de la date de fin de vie
 */
function onChangeEnd() {
    let serviceInput = $(".serviceDate");
    let endInput = $(".endLifeDate");
    let fabricationInput = $(".fabricationDate");

    let fabricationText = fabricationInput.val();
    let serviceText = serviceInput.val();
    let endText = endInput.val();

    let dateFabrication = new Date(fabricationText.split("-")[1], fabricationText.split("-")[0] - 1, 1);
    let serviceDate = new Date(serviceText.split("-")[1], serviceText.split("-")[0] - 1, 1);
    let endLifeDate = new Date(endText.split("-")[1], endText.split("-")[0] - 1, 1);

    /**
     * On met à jour la restriction pour la date de mise en service
     */
    serviceInput.datepicker("remove");
    serviceInput.datepicker({
        maxViewMode: 2,
        startView: 1,
        minViewMode: 1,
        language: "fr",
        autoclose: true,
        format: "mm-yyyy",
        todayHighlight: true,
        forceParse: 1,
        startDate: dateFabrication,
        endDate: endLifeDate,
    });

    /**
     * On vérifie si la date de mise en service a été saisie, pour la restriction de la date maximale possible pour la
     * date de fabrication
     */
    if (serviceText === "") {
        fabricationInput.datepicker("remove");
        fabricationInput.datepicker({
            format: "mm-yyyy",
            startView: 1,
            minViewMode: 1,
            maxViewMode: 2,
            language: "fr",
            autoclose: true,
            forceParse: 1,
            endDate: endLifeDate,
        });
    } else if (serviceDate < endLifeDate) {
        fabricationInput.datepicker("remove");
        fabricationInput.datepicker({
            format: "mm-yyyy",
            startView: 1,
            minViewMode: 1,
            maxViewMode: 2,
            language: "fr",
            autoclose: true,
            forceParse: 1,
            endDate: serviceDate,
        });
    }
}

/**
 * Fonction qui ajoute des champs en fonction du type d'EPI qui a été saisi
 */
function updateDisplayedFields() {
    let typeEpi = $('#epi').val();

    if (typeEpi === "Corde d'accès" || typeEpi === "Corde de rappel") {
        $("#row_for_more").html(
            "<div class=\"col-xs-12 col-sm-12 col-md-6\">" +
            "<label for=\"nombre_epissure\">Nombre d'épissure *</label>" +
            "<input type=\"number\" min=\"0\" id=\"nombre_epissure\" class=\"form-control mb-4\" placeholder=\"Nombre d'épissure de la corde\" onkeypress='return isNumber(event)'>" +
            "</div>" +
            "<div class=\"col-xs-12 col-sm-12 col-md-6\">" +
            "<label for=\"diametre\">Diamètre * (cm)</label>" +
            "<input type=\"number\" min=\"0\" step=\"any\" id=\"diametre\" class=\"form-control mb-4\" placeholder=\"Diamètre de la corde\" onkeypress=''>" +
            "</div>" +
            "<div class='col-xs-12 col-sm-12 col-md-6 offset-md-3'>" +
            "<label for=\"longueur_corde\">Longueur de la corde * (cm)</label>" +
            "<input type=\"number\" min=\"0\" step=\"1\" id=\"longueur_corde\" class=\"form-control mb-4\" placeholder=\"Longueur de la corde\" onkeypress='return isNumber(event)'>" +
            "</div>"
        );
    } else if (typeEpi === "Longe armée" || typeEpi === "Longe cordée") {
        $("#row_for_more").html(
            "<div class=\"col-xs-12 col-sm-6 col-md-6\">" +
            "<label for=\"numero_connecteur\">Numéro du tendeur *</label>" +
            "<input type=\"text\" id=\"num_tendeur\" class=\"form-control mb-4\" placeholder=\"Numéro du tendeur de la longe\">" +
            "</div>" +
            "<div class=\"col-xs-12 col-sm-6 col-md-6\">" +
            "<label for=\"numero_connecteur\">Numéro du connecteur *</label>" +
            "<input type=\"text\" id=\"numero_connecteur\" class=\"form-control mb-4\" placeholder=\"Numéro du connecteur de la longe\">" +
            "</div>"
        );
    } else {
        $("#row_for_more").html("");
    }
}

/**
 * Fonction qui charge les champs depuis la BDD
 */
function loadFields() {
    let groupName = $("#nom_groupe");

    /**
     * On charge les groupes/structures
     */
    $.get(
        '/scripts_requests/select_all_groups_without_stock.php',
        function (data) {
            let structures = JSON.parse(data);
            for (let i = 0; i < structures.length; i++) {
                let groupe = structures[i]['nom_groupe'];
                $("#nom_groupe").append('<option value="' + groupe +
                    '">' + groupe +
                    '</option>')
            }
        }
    ).then(
        /**
         * Puis, on charge les lots en fonction du premier groupe de la comboBox
         */
        function () {
            let groupe = groupName.val();
            $.post(
                '/scripts_requests/select_lots_by_group_including_stock.php',
                {
                    groupe: groupe,
                },
                function (data) {
                    let lots = JSON.parse(data);
                    for (let i = 0; i < lots.length; i++) {
                        let lot = lots[i]['nom_lot'];
                        $("#list_lots").append('<option value="' + lot +
                            '">' + lot +
                            '</option>')
                    }
                    $(".flexdatalistLot").flexdatalist({
                        minLength: 1,
                        searchContain: true,
                        focusFirstResult: true,
                        searchDelay: 100,
                        noResultsText: 'Aucun résultat trouvé pour "{keyword}"'
                    }).on('change:flexdatalist', function (event, set) {
                        lotExists(set.value);
                    })
                },
                'text'
            );

            /**
             * Lorsqu'un autre groupe est saisi, on met à jour le champ des lots en fonction de ce groupe
             */
            groupName.on('change', function () {
                let groupe = groupName.val();
                $.post(
                    '/scripts_requests/select_lots_by_group_including_stock.php',
                    {
                        groupe: groupe,
                    },
                    function (data) {
                        let lots = JSON.parse(data);
                        let list_lots = $("#list_lots");
                        let code_couleur = $("#code_couleur");

                        list_lots.empty();
                        code_couleur.removeAttr("disabled").val("");
                        for (let i = 0; i < lots.length; i++) {
                            let lot = lots[i]['nom_lot'];
                            list_lots.append('<option value="' + lot +
                                '">' + lot +
                                '</option>');
                            if (lot === $("#lot").val()) {
                                code_couleur.attr("disabled", "").val(lots[i]['code_couleur']);
                            }
                        }
                    },
                    'text'
                );
            });
        }
    );

    /**
     * On récupère les marques depuis la BD
     */
    $.post(
        '/scripts_requests/select_all_marques.php',
        function (data) {
            let marques = JSON.parse(data);
            if (marques.length === 0) {
                $("#marque").append("<option hidden disabled selected=''>Aucune marque enregistrée</option>");
            }
            for (let i = 0; i < marques.length; i++) {
                let marque = marques[i]['nom'];
                $("#marque").append('<option value="' + marque +
                    '">' + marque +
                    '</option>')
            }
            $(".flexdatalistMarque").flexdatalist({
                minLength: 1,
                searchContain: true,
                focusFirstResult: true,
                searchDelay: 100,
                noResultsText: 'Aucun résultat trouvé pour "{keyword}"'
            });
        },
        'text'
    );

    /**
     * On récupère les types d'EPI depuis la BD
     */
    $.get(
        '/scripts_requests/select_all_typeepi.php',
        function (data) {
            let types = JSON.parse(data);
            if (types.length === 0) {
                $("#epi").append("<option hidden disabled selected=''>Aucun type d'EPI enregistré</option>");
            }
            for (let i = 0; i < types.length; i++) {
                let type = types[i]['libelle'];
                $("#epi").append('<option value="' + type +
                    '">' + type +
                    '</option>')
            }
            updateDisplayedFields();
        },
        'text'
    );
}

/**
 * On vérifie lorsque le champs du lot a changé, si le lot existe dans la BD, dans ce cas on charge son code couleur,
 * sinon on ne fait rien
 * @param nameLot Le nom du lot
 */
function lotExists(nameLot) {
    let valeur = nameLot;
    let groupe = $("#nom_groupe").val();
    let option = $('option[value="' + valeur + '"').val();
    if (option === 'Stock' || option === "stock") {
        groupe = 'Stock';
    }

    /**
     * On vérifie l'existence grace à la datalist des lots, si le lots est dans cette liste, alors le lot existe dans la BD
     */
    if (option === valeur) {
        $("#code_couleur").attr("disabled", "");
        $.post(
            '/scripts_requests/select_lot_by_name_group.php',
            {
                nom_lot: valeur,
                groupe: groupe,
            },
            function (data) {
                let lot = JSON.parse(data);
                for (let i = 0; i < lot.length; i++)
                    $("#code_couleur").val(lot[i]['code_couleur']);
            },
            'text'
        )
    } else {
        $("#code_couleur").removeAttr("disabled").val("");
    }
}

/**
 * Fonction qui insère
 * @returns {boolean}
 */
function createEpi() {
    /**
     * On récupère toutes les informations de l'EPI
     */

    let groupe = $("#nom_groupe").val();

    let lot = $("#lot").val();
    let typeEpi = $("#epi").val();
    let numeroSerie = $("#numero_serie").val();
    let marque = $("#marqueInput").val();

    let fabricationDate = $("#annee_fabrication").val();
    let inServiceDate = $("#date_mise_en_service").val();
    let endDate = $("#fin_de_vie").val();
    let modele = $("#modele").val();
    let image = $("#file-with-current").prop("files")[0];
    let colorCode = $("#code_couleur").val();

    let cordeLength = $("#longueur_corde").val();
    let nbEpissure = $("#nombre_epissure").val();
    let diametre = $("#diametre").val();
    let tendeurReducteurNumber = $("#num_tendeur").val();
    let connecteurExtremiteNumber = $("#numero_connecteur").val();

    if (lot === 'Stock' || lot === "stock") {
        groupe = 'Stock';
    }

    if (typeEpi !== "Corde d'accès" && typeEpi !== "Corde de rappel") {
        diametre = null;
        cordeLength = null;
        nbEpissure = null;
    }

    if (typeEpi !== "Longe armée" && typeEpi !== "Longe cordée") {
        connecteurExtremiteNumber = null;
        tendeurReducteurNumber = null;
    }

    let form = new FormData();
    form.append("lot", lot);
    form.append("type_epi", typeEpi);
    form.append("numero_serie", numeroSerie);
    form.append("marque", marque);
    form.append("image", image);
    form.append("code_couleur", colorCode);
    form.append("annee_fabrication", fabricationDate);
    form.append("date_mise_en_service", inServiceDate);
    form.append("date_fin_vie", endDate);
    form.append("modele", modele);
    form.append("diametre", diametre);
    form.append("nb_epissure", nbEpissure);
    form.append("numero_connecteur_extremite", connecteurExtremiteNumber);
    form.append("longueur_corde", cordeLength);
    form.append("groupe", groupe);
    form.append("numero_tendeur_reducteur", tendeurReducteurNumber);
    form.append("isFile", "false");

    if (image !== undefined) {
        form.set("isFile", "true");
    }

    /**
     * On appelle le script PHP qui va réaliser l'insertion dans la BDD
     */
    $.blockUI({
        theme: false,
        baseZ: 2000,
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: '<h4><img alt="" src="../../assets/images/busy.gif" /><br><br> Insertion de l\'EPI en cours ...</h4>'
    });
    $.ajax({
        url: '/scripts_requests/insert_epi.php',
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        type: "POST",
        method: "POST",
        success: function (data, textStatus, jqXHR) {
            try {
                console.log(data);
                let insertion = JSON.parse(data);
                if (insertion['success'] !== true && (insertion['success']).includes("Duplicata")) {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("Un EPI avec ce numéro de série existe déjà");
                    });
                    return false;
                }

                /**
                 * On vérifie si le lot a bien été ajouté s'il n'existait pas
                 */
                if (insertion['insert'] !== undefined) {

                    if (insertion['insert'] !== true && (insertion['insert']).includes("Duplicata")) {
                        $.getScript("assets/js/utils.js", function () {
                            displayErrorMessage("Un lot avec ce code couleur existe déjà");
                        });
                        return false;
                    }

                    /**
                     * On vérifie si l'insertion a bien fonctionnée dans le cas où on a créé un lot
                     */
                    if (insertion['success'] === true) {
                        $.getScript("assets/js/utils.js", function () {
                            displayInfoMessage("Insertion de l'EPI et du nouveau lot réussie, vous allez être renvoyé vers la vérification");
                        });
                        $("#exampleModalPreview").modal("hide");
                        displayVerification(numeroSerie, typeEpi, lot, groupe, "insertionOkLot");
                    } else {
                        $.getScript("assets/js/utils.js", function () {
                            displayErrorMessage("Insertion de l'EPI non réussie, lot correctement créé");
                        });
                    }

                } else {
                    /**
                     * On vérifie si l'insertion a bien fonctionnée dans le cas où l'on a pas créé de lot
                     */
                    if (insertion['success'] === true) {
                        $.getScript("assets/js/utils.js", function () {
                            displayInfoMessage("Insertion de l'EPI réussie, vous allez être renvoyé vers la vérification");
                        });
                        $("#exampleModalPreview").modal("hide");
                        displayVerification(numeroSerie, typeEpi, lot, groupe, "insertionOk");
                    } else {
                        $.getScript("assets/js/utils.js", function () {
                            displayErrorMessage("Insertion de l'EPI non réussie");
                        });
                    }
                }
            } catch (e) {
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Erreur de connexion à la base de données");
                });
                console.log(e);
            }
        }
    }).then(function () {
        $.unblockUI();
    });
}

/**
 * On renvoie à la suite de l'insertion vers la vérification de cet EPI
 * @param numeroSerie
 * @param typeEpi
 * @param lot
 * @param groupe
 * @param message
 */
function displayVerification(numeroSerie, typeEpi, lot, groupe, message) {
    if (lot === 'Stock')
        groupe = 'Stock';
    window.location.href = "/verification?numero_serie=" + numeroSerie + "&type_epi=" + typeEpi + "&nom_lot=" + lot + "&groupe=" + groupe + "&message=" + message;
}