function executePost(marque, type_epi, fichier, modele, isFile) {
    $.post(
        '/scripts_requests/insert_notice.php',
        {
            marque: marque,
            type_epi: type_epi,
            message_notice: fichier,
            modele: modele,
            isFile: isFile,
        },
        function (data) {
            try {
                let result = JSON.parse(data);
                if (result['success'] === true) {
                    $.getScript("assets/js/utils.js", function () {
                        displayTemporaryInfoMessage("Insertion de la notice dans la base de données réussie")
                    });

                    loadMarques();

                    loadDatatable();

                    $("#marqueInput").val("");
                    $("#epi").val("");
                    $("#modele").val("");

                    $("#file-with-current").val("");
                    $(".span-choose-file").text("Choisir le PDF");
                    $("#lien").val("");
                } else {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("Insertion de la notice dans la base de données non réussie")
                    });
                }
            } catch (e) {
                console.log(e);
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Erreur lors de l'insertion de la notice dans la base de données")
                });
            }
        },
        'text'
    ).then(function () {
        $.unblockUI();
    });
}

/**
 * On envoie les données au serveur
 * @param formData le formdata contenant les données qui seront envoyées au serveur
 */
function executePostFile(formData) {
    $.ajax({
        url: '/scripts_requests/insert_notice.php',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: "POST",
        method: "POST",
        success: function (data, textStatus, jqXHR) {
            try {
                let result = JSON.parse(data);
                if (result['success'] === true) {
                    $.getScript("assets/js/utils.js", function () {
                        displayTemporaryInfoMessage("Insertion de la notice dans la base de données réussie")
                    });

                    loadMarques();

                    loadDatatable();

                    $("#marqueInput").val("");
                    $("#epi").val("");
                    $("#modele").val("");

                    $("#file-with-current").val("");
                    $(".span-choose-file").text("Choisir le PDF");
                    $("#lien").val("");
                } else {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("Insertion de la notice dans la base de données non réussie")
                    });
                }
            } catch (e) {
                console.log(e);
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Erreur lors de l'insertion de la notice dans la base de données")
                });
            }
        }
    }).then(function () {
        $.unblockUI();
    });
}

/**
 * Fonction d'ajout de notice dans la base de données
 * @returns {boolean}
 */
function addNotice() {
    let marque = $("#marqueInput").val();
    let typeEpi = $("#epi").val();
    let modele = $("#modele").val();

    let fichier = $("#file-with-current").prop("files")[0];

    let url = $("#lien").val();

    console.log(fichier);
    console.log(url);

    /**
     * On vérifie qu'au moins un des deux champs pour le fichier de la notice est saisi
     */
    if (fichier === undefined && url === "") {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("Vous n'avez pas sélectionné de fichier ou écrit un lien URL pour un PDF en ligne")
        });
        return false;
    }

    /**
     * On vérifie que les deux champs ne soient pas saisis
     */
    if (fichier !== undefined && url !== "") {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("Le fichier de la notice est soit un fichier importé soit un URL saisi")
        });
        return false;
    }

    if (url !== "") {
        let isUrl = url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        if (isUrl == null) {
            $.getScript("assets/js/utils.js", function () {
                displayErrorMessage("Vous n'avez pas saisi un URL valide");
            });
            return false;
        }
    }

    /**
     * On vérifie que les champs ne soient pas vides
     */
    if (marque === "" || typeEpi === "" || modele === "") {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("Vous n'avez pas saisi toutes les valeurs nécessaires à l'importation de la notice")
        });
        return false;
    }

    /**
     * Post pour l'ajout d'une notice dans la base de données
     */
    $.blockUI({
        theme: false,
        baseZ: 2000,
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: '<h4><img alt="" src="../../assets/images/busy.gif" /><br><br> Insertion de la notice en cours ...</h4>'
    });
    if (fichier !== undefined) {
        //executePost(marque, typeEpi, fichier, modele, "true");
        let form = new FormData();
        form.append("message_notice", fichier);
        form.append("marque", marque);
        form.append("type_epi", typeEpi);
        form.append("modele", modele);
        form.append("isFile", "true");

        executePostFile(form);
    } else {
        executePost(marque, typeEpi, url, modele, "false");
    }
}

/**
 * Fonction chargeant la liste les marques
 */
function loadMarques() {
    /**
     * On récupère la liste des marques afin d'instancier la datalist
     */
    $.get(
        '/scripts_requests/select_all_marques.php',
        function (data) {
            let marques = JSON.parse(data);

            let listMarques = $("#list_marques");
            listMarques.empty();
            for (let i = 0; i < marques.length; i++) {
                listMarques.append('<option value="' + marques[i]['nom'] +
                    '">' + marques[i]['nom'] +
                    '</option>')
            }

            $(".flexdatalist").flexdatalist({
                minLength: 1,
                searchContain: true,
                focusFirstResult: true,
                searchDelay: 100,
                noResultsText: 'Aucun résultat trouvé pour "{keyword}"'
            });
        },
        'text'
    );
}

/**
 * Fonction chargeant la table des notices
 */
function loadDatatable() {
    /**
     * On charge la table des notices
     */
    $("#table-marques").DataTable({
        "destroy": true,
        "ajax": {
            "url": "/scripts_requests/select_all_notices.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "responsive": true,
        "language": {
            "emptyTable": "Aucune notice à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ notices affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table des notices ...",
            "processing": "Recherche de notices ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ notices)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de notices correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });
}

/**
 * Fonction qui restreint en fonction de la touche tapée dans le champs URL ou de l'item sélectionné
 * le file file chooser
 */
function restrictFileName() {

}

/**
 * Fonction chargeant les types d'EPI
 */
function loadTypeEpi() {
    $.get(
        '/scripts_requests/select_all_typeepi.php',
        function (data) {
            let typesEpi = JSON.parse(data);

            for (let i = 0; i < typesEpi.length; i++) {
                $("#epi").append('<option value="' + typesEpi[i]['libelle'] +
                    '">' + typesEpi[i]['libelle'] +
                    '</option>')
            }
        },
        'text'
    )
}

$(document).ready(function () {

    loadDatatable();

    $("#submitButton").on("click", function () {
        addNotice();
    });

    /**
     * Chargement des types d'EPI dans la combo box
     */
    $.get(
        '/scripts_requests/select_all_typeepi.php',
        function (data) {
            let types_epi = JSON.parse(data);

            for (let i = 0; i < types_epi.length; i++) {
                $("#epi").append('<option value="' + types_epi[i]['libelle'] +
                    '">' + types_epi[i]['libelle'] +
                    '</option>')
            }
        },
        'text'
    );

    loadMarques();

    loadTypeEpi();

    /**
     * Lorsque l'on clique sur une ligne, on charge le pdf de la notice
     */
    $('#table-marques tbody').on('click', 'tr', function () {
        let notice = $(this).find('td').eq(3).text();
        let type = $(this).find('td').eq(1).text();
        let marque = $(this).find('td').eq(0).text();
        notice = notice.match(/^http?:/) ? notice : '//' + notice;
        if (marque !== "Aucune notice à afficher") {
            open(notice, "Notice de la marque " + marque + " pour le type d'EPI " + type);
        }
    });

    restrictFileName();

    let oldValueModele = "";
    $("#modele").on("keyup", function () {
        let modele = $("#modele").val();
        if (modele !== "" && oldValueModele !== modele) {
            let firstLetter = modele.charAt(0).toUpperCase();
            let string = modele.slice(1);

            oldValueModele = firstLetter + string;

            $("#modele").val(firstLetter + string);
        }
    });

    $("#file-with-current").on("change", function () {
        if ($("#file-with-current").prop("files")[0].size > 5242880) {
            $.getScript("assets/js/utils.js", function () {
                displayTemporaryErrorMessage("La taille de l'image est trop importante, 5Mo maximum");
            });
            $(this).val("");
            $(".span-choose-file").text("Choisir le PDF");
        }
    });

    setTimeout(function () {
        let oldValueMarque = "";
        $(".flexdatalist").on("change", function () {
            let marque = $(".flexdatalist").val();
            if (marque !== "" && oldValueMarque !== marque) {
                if (marque.length > 18)
                    marque = marque.substr(0, marque.length - 1);

                let firstLetter = marque.charAt(0).toUpperCase();
                let string = marque.slice(1);

                oldValueMarque = firstLetter + string;

                $(".flexdatalist").val(firstLetter + string);
            }
        });
    }, 300);
});