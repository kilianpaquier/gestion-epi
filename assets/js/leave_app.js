/**
 * Fonction qui appelle un script PHP afin de fermer la session et de se déconnecter de l'application
 */
function logout() {
    $.get(
        '/scripts_requests/destroy_session.php',
        {},
        function () {
            window.location.href = "/login";
        },
        'text'
    )
}

$(document).ready(function () {
   $.get(
       '/scripts_requests/select_session.php',
       function (data) {
           let session = JSON.parse(data);

           $("#profil").text(session['prenom'] + ' ' + session['nom']);
       }
   );

   setTimeout(
       function () {
           $(".dataTables_filter input").prop("placeholder", "Recherche")
       }, 300
   );

});