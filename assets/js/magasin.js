$(document).ready(function () {
    let lot = "Stock";
    let groupe = "Stock";

    /**
     * On charge la table des EPI du magasin
     */
    $('#table-epi').DataTable({
        "ajax": {
            "url": "/scripts_requests/dotation_table.php",
            "dataSrc": "",
            "data": {
                "lot": lot,
                "groupe": groupe,
            },
            "type": 'POST',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucun EPI à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ EPI affichés",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table des EPI ...",
            "processing": "Recherche des EPI ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ EPI)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas d'EPI correspondants à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });
});