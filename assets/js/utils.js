/**
 * Fonction qui compare deux mots de passe passés en paramètre
 * @param password
 * @param password2
 * @returns {boolean}
 */
function samePassword(password, password2) {
    if (password !== password2) {
        displayErrorMessage("Les mots de passe ne concordent pas");
        return false;
    } else {
        hideMessage();
        return true;
    }
}

/**
 * Fonction de validation d'un mail en temps que mail
 * @param mail
 * @returns {boolean}
 */
function validateEmail(mail) {
    return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(mail);
}

/**
 * Fonction affichant un message d'erreur
 * @param message
 */
function displayErrorMessage(message) {
    $("#success").removeClass().addClass("alert alert-danger text-danger text-center mb-2 pb-2 mt-2 font-weight-bold").html(message);
}

/**
 * Fonction affichant un message d'erreur temporaire
 * @param message
 */
function displayTemporaryErrorMessage(message) {
    $("#success").removeClass().addClass("alert alert-danger text-danger text-center mb-2 pb-2 mt-2 font-weight-bold").html(message);
    setTimeout(function () {
        hideMessage();
    }, 4000);
}

/**
 * Fonction affichant un message d'information temporaire (message de succès)
 * @param message
 */
function displayTemporaryInfoMessage(message) {
    $("#success").removeClass().addClass("alert alert-info text-info text-center mb-2 pb-2 mt-2 font-weight-bold").html(message);
    setTimeout(function () {
        hideMessage();
    }, 4000);
}

/**
 * Fonction affichant un message d'erreur dans un tagName donné
 * @param tagName La classe ou l'id dans lequel écrire le message
 * @param message
 */
function customErrorMessage(tagName, message) {
    $(tagName).removeClass().addClass("alert alert-danger text-danger text-center mb-2 pb-2 mt-2 font-weight-bold").html(message);
    setTimeout(function () {
        hideCustomMessage(tagName);
    }, 4000);
}

/**
 * Fonction remettant à vide un message précédemment écrit dans le tagName
 * @param tagName
 */
function hideCustomMessage(tagName) {
    $(tagName).removeClass().html("");
}

/**
 * Fonction affichant un message d'information (message de succès)
 * @param message
 */
function displayInfoMessage(message) {
    $("#success").removeClass().addClass("alert alert-info text-info text-center mb-2 pb-2 mt-2 font-weight-bold").html(message);
}

/**
 * Fonction remettant le champ message à vide
 */
function hideMessage() {
    $("#success").removeClass().addClass().html("");
}