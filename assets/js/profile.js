function loadModificationComptes(certificat) {
    window.location.href = "/modification-compte?certificat=" + certificat;
}

/**
 * Fonction qui charge la table des comptes dans le cas d'un administrateur
 */
function loadComptes() {
    $("#table-comptes").append(
        '<div class="row">' +
        '<div class="col-12">' +
        '<div class="border border-light pr-5 pl-5 pb-3">' +
        '<table class="table table-hover table-responsive-md table-striped text-nowrap w-100 display" id="table">' +
        '<thead>' +
        '<tr>' +
        '<th>Nom</th>' +
        '<th>Prénom</th>' +
        '<th>Identifiant</th>' +
        '</tr>' +
        '</thead>' +
        '</table>' +
        '</div>' +
        '</div>' +
        '</div>'
    );

    /**
     * On charge la table
     */
    $('#table').DataTable({
        "ajax": {
            "url": "/scripts_requests/select_all_accounts.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucun compte à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ comptes affichés",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table des comptes ...",
            "processing": "Recherche de comptes ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ comptes)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de comptes correspondants à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    $('#table tbody').on('click', 'tr', function () {
        loadModificationComptes($(this).find("td").eq(2).text());
    });
}

$(document).ready(function () {
    let url = new URL(window.location);

    if (url.searchParams.get("suppression") !== undefined) {
        if (url.searchParams.get("suppression") === "success") {
            $.getScript("assets/js/utils.js", function () {
                displayTemporaryInfoMessage("Compte correctement supprimé");
            });
        }
    }

    if (url.searchParams.get("modification") !== undefined) {
        if (url.searchParams.get("modification") === "success") {
            $.getScript("assets/js/utils.js", function () {
                displayTemporaryInfoMessage("Compte correctement modifié");
            });
        }
    }

    if (url.searchParams.get("creation") !== undefined) {
        if (url.searchParams.get("creation") === "success") {
            $.getScript("assets/js/utils.js", function () {
                displayTemporaryInfoMessage("Compte correctement créé");
            });
        }
    }

    if (url.searchParams.get("changement-mdp") !== undefined) {
        if (url.searchParams.get("changement-mdp") === "success") {
            $.getScript("assets/js/utils.js", function () {
                displayTemporaryInfoMessage("Votre mot de passe a bien été modifié");
            });
        }
    }

    /**
     * On récupère les données de la session
     */
    $.post(
        '/scripts_requests/select_session.php',
        function (data) {
            try {
                let session = JSON.parse(data);
                let nom = session['nom'];
                let prenom = session['prenom'];
                let mail = session['mail'];
                let certificat = session['certificat'];
                $("#nom").text(nom);
                $("#prenom").text(prenom);
                $("#certificat").text(certificat);
                $("#mail").text(mail);

                /**
                 * Dans le cas d'une session administrateur on charge des données en plus
                 */
                if (session['permission'] === "0") {
                    $("#buttons").append("        <div class=\"col-xs-12 col-sm-12 col-md-6\">\n" +
                        "            <a class=\"btn special-color btn-block text-white my-4\" href=\"/creation-compte\">Créer un compte vérificateur\n" +
                        "            </a>\n" +
                        "        </div>");

                    loadComptes();
                }
            } catch (e) {
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Impossible de charger les données de la session");
                });
                console.log(e);
            }
        },
        'text'
    )
});