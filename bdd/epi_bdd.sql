-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 30 Avril 2019 à 15:30
-- Version du serveur :  5.7.25-0ubuntu0.16.04.2
-- Version de PHP :  7.0.33-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `epi_bdd`
--
CREATE DATABASE IF NOT EXISTS `epi_bdd` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `epi_bdd`;

-- --------------------------------------------------------

--
-- Structure de la table `controles`
--

DROP TABLE IF EXISTS `controles`;
CREATE TABLE `controles`
(
  `etat`              varchar(50)  NOT NULL,
  `image`             varchar(500) DEFAULT NULL,
  `observations`      varchar(500) DEFAULT NULL,
  `point_de_controle` int(11)      NOT NULL,
  `date_verification` varchar(50)  NOT NULL,
  `epi_verification`  varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `epi`
--

DROP TABLE IF EXISTS `epi`;
CREATE TABLE `epi`
(
  `numero_serie`                varchar(255) NOT NULL,
  `marque`                      varchar(50)  NOT NULL,
  `modele`                      varchar(50)  NOT NULL,
  `annee_fabrication`           varchar(50)  NOT NULL,
  `date_mise_en_service`        varchar(50)  NOT NULL,
  `date_fin_de_vie`             varchar(50)  DEFAULT NULL,
  `diametre`                    float        DEFAULT NULL,
  `nb_epissure`                 int(11)      DEFAULT NULL,
  `numero_tendeur_reducteur`    varchar(50)  DEFAULT NULL,
  `numero_connecteur_extremite` varchar(50)  DEFAULT NULL,
  `image`                       varchar(500) DEFAULT NULL,
  `longueur_corde`              int(11)      DEFAULT NULL,
  `lot`                         varchar(50)  NOT NULL,
  `type_epi`                    varchar(50)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE `groupe`
(
  `nom_groupe` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `lot`
--

DROP TABLE IF EXISTS `lot`;
CREATE TABLE `lot`
(
  `nom_lot`      varchar(50)  NOT NULL,
  `code_couleur` varchar(255) NOT NULL,
  `groupe`       varchar(50)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE `marque`
(
  `nom` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`
(
  `message_notice` varchar(500) NOT NULL,
  `type_epi`       varchar(50)  NOT NULL,
  `modele`         varchar(50)  NOT NULL,
  `marque`         varchar(50)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `point_de_controle`
--

DROP TABLE IF EXISTS `point_de_controle`;
CREATE TABLE `point_de_controle`
(
  `id_point_de_controle` int(11)      NOT NULL,
  `libelle`              varchar(255) NOT NULL,
  `type_epi`             varchar(50)  NOT NULL,
  `type_verification`    varchar(50)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_epi`
--

DROP TABLE IF EXISTS `type_epi`;
CREATE TABLE `type_epi`
(
  `libelle` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `verificateurs`
--

DROP TABLE IF EXISTS `verificateurs`;
CREATE TABLE `verificateurs`
(
  `certificat`   varchar(255) NOT NULL,
  `nom`          varchar(50)  NOT NULL,
  `prenom`       varchar(50)  NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `mail`         varchar(50)  NOT NULL,
  `permission`   int(11) DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `verifications`
--

DROP TABLE IF EXISTS `verifications`;
CREATE TABLE `verifications`
(
  `date_verification` varchar(50)  NOT NULL,
  `observations`      varchar(500) DEFAULT NULL,
  `etat`              varchar(50)  NOT NULL,
  `epi`               varchar(255) NOT NULL,
  `verificateur`      varchar(255) DEFAULT NULL,
  `url_pdf`           varchar(500) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `suppression_epi`
--

DROP TABLE IF EXISTS `suppression_epi`;
CREATE TABLE `suppression_epi`
(
  `Id`       int(11)      NOT NULL,
  `url_file` varchar(500) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE `suppression_epi`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `suppression_epi`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `controles`
--
ALTER TABLE `controles`
  ADD PRIMARY KEY (`point_de_controle`, `date_verification`, `epi_verification`),
  ADD KEY `verification_controle_contrainte` (`date_verification`, `epi_verification`);

--
-- Index pour la table `epi`
--
ALTER TABLE `epi`
  ADD PRIMARY KEY (`numero_serie`),
  ADD KEY `lot_epi_contrainte` (`lot`),
  ADD KEY `marque_epi_contrainte` (`marque`),
  ADD KEY `typeepi_epi_contrainte` (`type_epi`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`nom_groupe`);

--
-- Index pour la table `lot`
--
ALTER TABLE `lot`
  ADD PRIMARY KEY (`nom_lot`),
  ADD KEY `groupe_lot_contrainte` (`groupe`);

--
-- Index pour la table `marque`
--
ALTER TABLE `marque`
  ADD PRIMARY KEY (`nom`);

--
-- Index pour la table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`type_epi`, `marque`, `modele`),
  ADD KEY `type_epi` (`type_epi`),
  ADD KEY `marque` (`marque`);

--
-- Index pour la table `point_de_controle`
--
ALTER TABLE `point_de_controle`
  ADD PRIMARY KEY (`id_point_de_controle`),
  ADD KEY `type_epi_point_controle_contrainte` (`type_epi`);

--
-- Index pour la table `type_epi`
--
ALTER TABLE `type_epi`
  ADD PRIMARY KEY (`libelle`);

--
-- Index pour la table `verificateurs`
--
ALTER TABLE `verificateurs`
  ADD PRIMARY KEY (`certificat`),
  ADD UNIQUE KEY `verificateurs_mail_uindex` (`mail`);

--
-- Index pour la table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`date_verification`, `epi`),
  ADD KEY `verificateur_verification_contrainte` (`verificateur`),
  ADD KEY `epi_verification_contrainte` (`epi`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `point_de_controle`
--
ALTER TABLE `point_de_controle`
  MODIFY `id_point_de_controle` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `controles`
--
ALTER TABLE `controles`
  ADD CONSTRAINT `pointcontrole_controle_contrainte` FOREIGN KEY (`point_de_controle`) REFERENCES `point_de_controle` (`id_point_de_controle`),
  ADD CONSTRAINT `verification_controle_contrainte` FOREIGN KEY (`date_verification`, `epi_verification`) REFERENCES `verifications` (`date_verification`, `epi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `epi`
--
ALTER TABLE `epi`
  ADD CONSTRAINT `epi_lot_contrainte` FOREIGN KEY (`lot`) REFERENCES `lot` (`nom_lot`) ON UPDATE CASCADE,
  ADD CONSTRAINT `epi_marque_contrainte` FOREIGN KEY (`marque`) REFERENCES `marque` (`nom`) ON UPDATE CASCADE,
  ADD CONSTRAINT `epi_type_contrainte` FOREIGN KEY (`type_epi`) REFERENCES `type_epi` (`libelle`);

--
-- Contraintes pour la table `lot`
--
ALTER TABLE `lot`
  ADD CONSTRAINT `groupe_lot_contrainte` FOREIGN KEY (`groupe`) REFERENCES `groupe` (`nom_groupe`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `notice`
--
ALTER TABLE `notice`
  ADD CONSTRAINT `notice_marque` FOREIGN KEY (`marque`) REFERENCES `marque` (`nom`),
  ADD CONSTRAINT `notice_type_epi` FOREIGN KEY (`type_epi`) REFERENCES `type_epi` (`libelle`);

--
-- Contraintes pour la table `point_de_controle`
--
ALTER TABLE `point_de_controle`
  ADD CONSTRAINT `type_epi_point_controle_contrainte` FOREIGN KEY (`type_epi`) REFERENCES `type_epi` (`libelle`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `verifications`
--
ALTER TABLE `verifications`
  ADD CONSTRAINT `epi_verification_contrainte` FOREIGN KEY (`epi`) REFERENCES `epi` (`numero_serie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `verificateur_verification_contrainte` FOREIGN KEY (`verificateur`) REFERENCES `verificateurs` (`certificat`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
