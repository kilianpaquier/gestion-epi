<header>
    <nav class="navbar navbar-expand-xl navbar-dark special-color">
        <a class="navbar-brand" href="/index">Accueil</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/insertion">Enregistrement d'un EPI</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/verification">Vérifications</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/notices">Notices</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/stock">Gestion du stock</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lots">Liste des lots</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/synthese">Tableaux de synthèse</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/profil" id="profil"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="logout()">Déconnexion</a>
                </li>
            </ul>
        </div>
    </nav>
</header>