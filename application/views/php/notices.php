<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Tableau des notices</title>

    <?php include("head.php"); ?>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

    <!--    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">-->
    <!--    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">-->

</head>

<?php include("nav.php"); ?>

<body>
<div class="container pt-5 animated fadeIn mt-5 mb-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des notices</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="border border-light pr-5 pl-5 pb-3">
                    <table class="table table-hover w-100 dt-responsive table-responsive-lg table-striped text-nowrap display"
                           id="table-marques">
                        <thead>
                        <tr>
                            <th>Marque</th>
                            <th>Type d'EPI</th>
                            <th>Modèle</th>
                            <th>Lien PDF</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 float-right">
                    <button class="btn special-color btn-block text-white my-4" id="showButton" data-toggle="modal"
                            data-target="#exampleModalPreview">
                        Ajouter une notice
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<div class="modal fade fadeInDown" id="exampleModalPreview" tabindex="-1"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content p-5">
            <div class="modal-header mt-n5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="header animated zoomIn" align="center">
                <h3 class="panel-title">Ajout d'une notice</h3>
            </div>
            <div class="contentText animated zoomIn">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <label for="marqueInput">Marque *</label>
                            <input type="text" id="marqueInput" class="form-control mb-4 flexdatalist" list="list_marques"
                                   placeholder="Saisir la marque" required>
                            <datalist id="list_marques">

                            </datalist>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="epi">Type d'EPI *</label>
                            <select class="browser-default custom-select mb-4" id="epi" required>
                                <option hidden value="">Choisir le type...</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="modele">Modèle *</label>
                            <input type="text" id="modele" class="form-control mb-4" placeholder="Saisir le modèle"
                                   required maxlength="16">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="row-12">
                                <label for="notice">Notice *</label>
                                <input type="url" id="lien" class="form-control" placeholder="Saisir l'URL">
                            </div>
                            <div class="row-12 text-center mt-2">
                                <label>OU</label>
                            </div>
                            <div class="row-12">
                                <div class="input-default-wrapper">
                                    <input type="file" id="file-with-current" class="input-default-js"
                                           accept="application/pdf">
                                    <label class="label-for-default-js rounded bg-white" for="file-with-current">
                                        <span class="span-choose-file">Choisir le PDF</span>
                                        <div class="float-right span-browse">Rechercher</div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-12">
                        <div class="col-xs-12 mb-n4 col-sm-12 col-md-12 col-lg-12">
                            <button class="btn special-color btn-block text-white mt-5 mb-5"
                                    id="submitButton" type="submit" onclick="">
                                Ajouter la notice
                            </button>
                        </div>
                    </div>
                    <div class="col-12">
                        <p id="success"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/notices.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
        type="text/javascript"></script>

</html>