<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Profil</title>

    <?php include("head.php"); ?>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

</head>

<?php include("nav.php"); ?>

<body>
<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Profil</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="nom">Nom : </label>
                <label id="nom"></label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="prenom">Prénom : </label>
                <label id="prenom"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="certificat">Identifiant : </label>
                <label id="certificat"></label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="mail">Email : </label>
                <label id="mail"></label>
            </div>
        </div>

        <div class="row" id="buttons">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <a class="btn special-color btn-block text-white my-4" href="/changement-mdp">Changer de mot de passe
                </a>
            </div>
        </div>

        <div id="table-comptes">

        </div>

        <div class="row">
            <div class="col-12">
                <p id="success"></p>
            </div>
        </div>
    </div>
</div>

</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/profile.js" type="text/javascript"></script>
</html>