<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 24/05/2019
 * Time: 09:07
 */

class CustomErrors extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function error403()
    {
        $this->load->helper('url');
        $this->output->set_status_header('403');
        $this->load->view('errors/html/error_403');
    }
    public function error401()
    {
        $this->load->helper('url');
        $this->output->set_status_header('401');
        $this->load->view('errors/html/error_401');
    }
    public function error400()
    {
        $this->load->helper('url');
        $this->output->set_status_header('400');
        $this->load->view('errors/html/error_400');
    }
    public function error500()
    {
        $this->load->helper('url');
        $this->output->set_status_header('500');
        $this->load->view('errors/html/error_500');
    }
    public function error501()
    {
        $this->load->helper('url');
        $this->output->set_status_header('501');
        $this->load->view('errors/html/error_501');
    }
    public function error502()
    {
        $this->load->helper('url');
        $this->output->set_status_header('502');
        $this->load->view('errors/html/error_502');
    }
    public function error503()
    {
        $this->load->helper('url');
        $this->output->set_status_header('503');
        $this->load->view('errors/html/error_503');
    }
    public function error504()
    {
        $this->load->helper('url');
        $this->output->set_status_header('504');
        $this->load->view('errors/html/error_504');
    }
    public function error505()
    {
        $this->load->helper('url');
        $this->output->set_status_header('505');
        $this->load->view('errors/html/error_505');
    }
    public function error520()
    {
        $this->load->helper('url');
        $this->load->view('errors/html/error_520');
    }
    public function error521()
    {
        $this->load->helper('url');
        $this->load->view('errors/html/error_521');
    }
}