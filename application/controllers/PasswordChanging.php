<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 13/05/2019
 * Time: 12:14
 */

class PasswordChanging extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        $this->load->view('php/password-changing');
    }
}