<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 07/05/2019
 * Time: 15:10
 */
class Dotation extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        $this->load->view('php/dotation');
    }
}