<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 23/04/2019
 * Time: 14:18
 */
class Insertion extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        $this->load->view('/php/insertion-epi');
    }
}